## Zaika - Pelmeni Timer

#### Простой таймер в 7 минут для варки пельменей с звуковым оповещением

#### Для работы требуется python версии 3+

### Установка:

``` git clone https://gitlab.com/DigitalMonroe/zaika-pelmeni-timer && pip install -r requirements.txt ```

### Запуск:

``` ./main.py ```
